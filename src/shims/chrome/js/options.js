    var sorter = function(a, b) {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    };
    if (chrome.extension) {
        var port = chrome.extension.connect({ name: "Browser Cleaner" });
        var onMessage = function(request) {
            if (!request.message) return;
            switch (request.message) {
                case 'extensions-response':
                    {
                        if (!request.value)
                            return;
                        ext_list = [];
                        var extensions = request.value;
                        extensions.sort(sorter);
                        $('#extensions li').remove();
                        expandExtensions(extensions);
                    }
                    break;
                case 'apps-response':
                    {
                        if (!request.value)
                            return;
                        apps_list = [];
                        var apps = request.value;
                        apps.sort(sorter);
                        $('#apps li').remove();
                        expandApps(apps);
                    }
                    break;
                case 'history-response':
                    {
                        if (!request.value)
                            return;
                        var history = request.value;
                        $("#history li").remove();
                        $('#history').append(expandHistory(history));
                    }
                    break;
                case 'autoclean-response':
                    {
                        var sHistory, sDownloads, sCache, sCookies = false;
                        if (request && request.data) {
                            sHistory = request.data.history.state;
                            sDownloads = request.data.downloads.state;
                            sCache = request.data.cache.state;
                            sCookies = request.data.cookies.state;
                        }
                        $("#autoclean-history").data('state', sHistory);
                        $("#autoclean-downloads").data('state', sDownloads);
                        $("#autoclean-cache").data('state', sCache);
                        $("#autoclean-cookies").data('state', sCookies);
                        if(sHistory === 'enabled') {
                            $("#autoclean-history").attr('checked', 'enabled');
                        }
                        if(sDownloads === 'enabled') {
                            $("#autoclean-downloads").attr('checked', 'enabled');
                        }
                        if(sCache === 'enabled') {
                            $("#autoclean-cache").attr('checked', 'enabled');
                        }
                        if(sCookies === 'enabled') {
                            $("#autoclean-cookies").attr('checked', 'enabled');
                        }
                    }
                    break;
                case 'bookmark-response':
                    {
                        if (!request.value)
                            return;
                        var bookmarkNodes = request.value;
                        $('#bookmarks li').remove();
                        $('#bookmarks-results li').remove();
                        bookmarks_mirror = [];
                        expandBookmarks(bookmarkNodes);
                    }
                    break;
                case 'clean-response':
                    {
                        $('.pendingWindow').html('Clean Successfully Completed!<br><button class="clean-panel-ok-button">OK</button>');
                        $('.clean-panel-ok-button').unbind().click(function(e) {
                            $('.pendingWindow').hide();
                        });
                    }
                    break;
            }
        };
        port.onMessage.addListener(onMessage);
    }

    function canDisable(list) {
        for (var index in list) {
            if (list[index].name === "Browser Cleaner")
                continue;
            if (list[index].enabled) return true;
        }

        return false;
    }

    function canRemove(list) {
        return list.length > 0;
    }
    var ext_list = [];
    var apps_list = [];
    var ext_list_mirror = [];
    var apps_list_mirror = [];
    var bookmarks_mirror = [];

    function refreshExtensionsControl() {
        if (canRemove(ext_list)) {
            $("#remove-ext-crtl").removeClass('disabled-control');
            $("#remove-ext-crtl > i").removeClass('disabled-control');
        } else {
            $("#remove-ext-crtl").addClass('disabled-control');
            $("#remove-ext-crtl > i").addClass('disabled-control');
        }

        if (canDisable(ext_list)) {
            $("#disable-ext-crtl").removeClass('disabled-control');
            $("#disable-ext-crtl > i").removeClass('disabled-control');
            $("#temp-ext-crtl").removeClass('disabled-control');
            $("#temp-ext-crtl > i").removeClass('disabled-control');
        } else {
            $("#disable-ext-crtl").addClass('disabled-control');
            $("#disable-ext-crtl > i").addClass('disabled-control');
            if (ext_list_mirror.length === 0) {
                $("#temp-ext-crtl").addClass('disabled-control');
                $("#temp-ext-crtl > i").addClass('disabled-control');
            }
        }
    }

    function refreshAppsControl() {
        if (canRemove(apps_list)) {
            $("#remove-apps-crtl").removeClass('disabled-control');
            $("#remove-apps-crtl > i").removeClass('disabled-control');
        } else {
            $("#remove-apps-crtl").addClass('disabled-control');
            $("#remove-apps-crtl > i").addClass('disabled-control');
        }

        if (canDisable(apps_list)) {
            $("#disable-apps-crtl").removeClass('disabled-control');
            $("#disable-apps-crtl > i").removeClass('disabled-control');
            $("#temp-apps-crtl").removeClass('disabled-control');
            $("#temp-apps-crtl > i").removeClass('disabled-control');
        } else {
            $("#disable-apps-crtl").addClass('disabled-control');
            $("#disable-apps-crtl > i").addClass('disabled-control');
            if (apps_list_mirror.length === 0) {
                $("#temp-apps-crtl").addClass('disabled-control');
                $("#temp-apps-crtl > i").addClass('disabled-control');
            }
        }
    }

    function setExtensionState(extId, state) {
        for (var index in ext_list) {
            if (ext_list[index].id == extId) {
                break;
            }
        }

        if (index < ext_list.length) {
            switch (state) {
                case 'enabled':
                    ext_list[index].enabled = true;
                    break;
                case 'disabled':
                    ext_list[index].enabled = false;
                    break;
                case 'deleted':
                    ext_list.splice(index, 1);
                    break;
            }
        }
        refreshExtensionsControl();
    }

    function setAppState(appsId, state) {
        for (var index in apps_list) {
            if (apps_list[index].id == appsId) {
                break;
            }
        }

        if (index < apps_list.length) {
            switch (state) {
                case 'enabled':
                    apps_list[index].enabled = true;
                    break;
                case 'disabled':
                    apps_list[index].enabled = false;
                    break;
                case 'deleted':
                    apps_list.splice(index, 1);
                    break;
            }
        }
        refreshAppsControl();
    }

    function toggleTempExtensions() {
        if (!canDisable(ext_list) && ext_list_mirror.length === 0) return;

        if (ext_list_mirror.length > 0) {
            $("#temp-ext-crtl span").text('Disable temporarily');
            $("#temp-ext-crtl > i").removeClass('fa-plus-circle');
            $("#temp-ext-crtl > i").addClass('fa-minus-circle');
            for (var i in ext_list_mirror) {
                ext_list[i].enabled = ext_list_mirror[i];
            }

            ext_list_mirror = [];
            renderTemplate('#ext-listing', '#ext-template', ext_list);
            refreshExtensionsControl();
        } else {
            for (var j in ext_list) {
                ext_list_mirror.push(ext_list[j].enabled);
            }
            $("#temp-ext-crtl span").text('Restore');
            $("#temp-ext-crtl > i").removeClass('fa-minus-circle');
            $("#temp-ext-crtl > i").addClass('fa-plus-circle');
            disableAllExtensions();
        }
    }

    function expandExtensions(extensions) {
        for (i = 0; i < extensions.length; i++) {
            if (extensions[i].name === "Browser Cleaner")
                continue;
            var extId = extensions[i].id;
            var extType = extensions[i].type;
            ext_list.push(extensions[i]);
            var extEnabled = extensions[i].enabled;
            var className = "disabled";
            var checked = "false";
            if (extEnabled) {
                className = "enabled";
                checked = "checked";
            }
            html = ' <li id="' + extId + '">';
            if (extensions[i].icons) {
                html += '<div><img src="' + extensions[i].icons[0].url + '" class="' + className + '-icon">';
            } else {
                html += '<div><i class="fa fa-file" aria-hidden="true"></i>';
            }
            html += '<span class="ext-title ' + className + '-title">' + extensions[i].name + '</span>';
            html += '<span class="action-button">';
            html += '<label class="toggle-switch">';
            html += '<input data-ext="' + extId + '" type="checkbox"';
            if (extEnabled) {
                html += ' checked>';
            } else {
                html += '>';
            }
            html += '<span class="slider"></span>';
            html += '</label>';
            html += '<label data-ext="' + extId + '" class="delete-control"></label>';
            html += '</span></div></li>';
            $("#extensions").append(html);
        }
        refreshExtensionsControl();
    }

    function expandApps(apps) {
        for (i = 0; i < apps.length; i++) {
            if (apps[i].name === "Browser Cleaner")
                continue;
            var extId = apps[i].id;
            var extType = apps[i].type;
            apps_list.push(apps[i]);
            var extEnabled = apps[i].enabled;
            var className = "disabled";
            var checked = "false";
            if (extEnabled) {
                className = "enabled";
                checked = "checked";
            }
            html = ' <li id="' + extId + '">';
            if (apps[i].icons) {
                html += '<div><img src="' + apps[i].icons[0].url + '" class="' + className + '-icon">';
            } else {
                html += '<div><i class="fa fa-file" aria-hidden="true"></i>';
            }
            html += '<span class="ext-title ' + className + '-title">' + apps[i].name + '</span>';
            html += '<span class="action-button"><label class="toggle-switch">';
            html += '<input data-ext="' + extId + '" type="checkbox"';
            if (extEnabled) {
                html += ' checked>';
            } else {
                html += '>';
            }
            html += '<span class="slider"></span>';
            html += '</label>';
            html += '<label data-ext="' + extId + '" class="delete-control"></label>';
            html += '</span></div></li>';
            $("#apps").append(html);
        }
        refreshAppsControl();
    }

    function expandHistory(history) {
        var ret = '';
        history_mirror = [];

        for (var idx in history) {
            var page = history[idx];
            var url = page.url;
            if (url.indexOf("chrome-extension://") !== -1 &&
                url.indexOf("chrome://") !== -1) {
                continue;
            }
            var title = page.title;
            if (title === "")
                title = url;
            if (title.length > 60) {
                title = title.substring(0, 60) + "...";
            }
            title = title.replace("<", "&lt;").replace(">", "&gt;");
            ret += '<li><div>';
            ret += '<img style="margin-top:0px" src="chrome://favicon/size/16@1x/' + url + '"/>';
            ret += '<a target="_blank" focus-type="title" href="' + url + '">';
            ret += '<span class="ext-title">' + title + '</span></a>';
            ret += '<span class="action-button"><label data-ext="' + url + '" class="x-control"></label></span>';
            ret += '</div></li>';
            history_mirror.push(page);
        }
        if (history_mirror.length === 0) {
            $("#clearHistory").addClass('disabled-control');
            $("#clearHistory > i").addClass('disabled-control');
        }
        return ret;
    }

    function expandBookmarks(bookmarks) {
        bookmarks.forEach(function(children) {
            if (children.hasOwnProperty("title") && !children.hasOwnProperty("url")) {
                title = children.title;
                var id = "bookmarks" + children.id;
                if (title !== "") {
                    if (title.length > 60) {
                        title = title.substring(0, 60) + "...";
                    }
                    html = '<li><label class="chevron-switch"><input name="bookmarks" id="bar-book-check' + children.id + '" type="checkbox"><span class="chevron"></span>';
                    html += '<span class="chevron-text" >' + title + '</span></label>';
                    html += '<ul id="' + id + '"  class="action-listing" style="margin-left: 10px;margin-top:-10px; display: none;">';
                    html += '<li><span class="ext-title" style="color:#c2c2c2 !important;">No Bookmarks</span></li>';
                    html += '</ul></li>';
                    $('#bookmarks').append(html);
                    $('#bar-book-check' + children.id).change(function() {
                        if ($(this).is(":checked")) {
                            $('#bookmarks' + children.id).show();
                        } else {
                            $('#bookmarks' + children.id).hide();
                        }
                    });
                }
            }
            if (typeof children.children == "object") {
                if (children.children.length > 0) {
                    $('#bookmarks' + children.id + ' li ').remove();
                }
                expandBookmarks(children.children);
            }
            if (children.hasOwnProperty("url")) {
                bookmarks_mirror.push(children);
                var url = children.url;
                title = children.title;
                if (title.length > 60) {
                    title = title.substring(0, 60) + "...";
                }
                var cid = children.id;
                var parentId = "bookmarks" + children.parentId;
                var ret = '<li><div>';
                ret += '<img style="margin-top:0px" src="chrome://favicon/size/16@1x/' + url + '"/>';
                ret += '<a target="_blank" focus-type="title" href="' + url + '">';
                ret += '<span class="ext-title">' + title + '</span></a>';
                ret += '<span class="action-button"><label id="' + cid + '" data-ext="' + url + '" class="x-control"></label></span>';
                ret += '</div></li>';
                if ($('#' + parentId).length > 0) {
                    //$('#' + parentId + " li").remove();
                    $('#' + parentId).append(ret);
                } else {
                    $('#bookmarks-results').append(ret);
                }
            }
        });
        if (bookmarks_mirror.length === 0) {
            $("#removeAllBookmarks").addClass('disabled-control');
            $("#removeAllBookmarks > i").addClass('disabled-control');
        }

    }

    $(document).ready(function() {
        window.addEventListener("contextmenu", function(e) { e.preventDefault(); });
        if (port) {
            port.postMessage({ message: "get-extensions", filter: '' });
            port.postMessage({ message: "get-apps", filter: '' });
            port.postMessage({ message: "get-history", filter: '' });
            port.postMessage({ message: "get-bookmarks" });
            port.postMessage({ message: "get-autoclean" });
        }
        $('#contact_us').unbind().click(function(e) {
            window.open("https://www.browsercleaner.com/contact-browsercleaner/");
        });

        $('#privacy').unbind().click(function(e) {
            window.open("https://www.browsercleaner.com/privacy/");
        });

        $('#' + $('input[name=tabs]:checked').data('section')).show();
        $('input[name=tabs]').change(function() {
            $('section:visible').hide();
            $('#' + $(this).data('section')).show();
            port.postMessage({ message: "get" + $(this).data("section").replace("content", ""), filter: '' });
        });

        $('#remove-ext-crtl').unbind().click(function(e) {
            port.postMessage({ message: 'remove-all-extensions' });
        });

        $('#disable-ext-crtl').unbind().click(function(e) {
            port.postMessage({ message: 'disable-all-extensions', type: 'extension' });
        });

        $('#temp-ext-crtl').unbind().click(function(e) {
            if (ext_list_mirror.length > 0) {
                $("#temp-ext-crtl span").text('Disable temporarily');
                $("#temp-ext-crtl > i").removeClass('fa-plus-circle');
                $("#temp-ext-crtl > i").addClass('fa-minus-circle');
                for (var i in ext_list_mirror) {
                    ext_list[i].enabled = ext_list_mirror[i];
                }

                ext_list_mirror = [];
                refreshExtensionsControl();
                port.postMessage({ message: 'toggle-temp-extensions', type: 'extension', state: 'restore' });
            } else {
                for (var j in ext_list) {
                    ext_list_mirror.push(ext_list[j].enabled);
                }
                $("#temp-ext-crtl span").text('Restore');
                $("#temp-ext-crtl > i").removeClass('fa-minus-circle');
                $("#temp-ext-crtl > i").addClass('fa-plus-circle');
                port.postMessage({ message: 'toggle-temp-extensions', type: 'extension', state: 'disable' });
            }
        });

        $('#searchExtension').on('input', function(e) {
            filter = e.target.value;
            port.postMessage({ message: "get-extensions", filter: filter });
        });

        $('#remove-apps-crtl').unbind().click(function(e) {
            port.postMessage({ message: 'remove-all-apps' });
        });

        $('#disable-apps-crtl').unbind().click(function(e) {
            port.postMessage({ message: 'disable-all-extensions', type: 'app' });
        });

        $('#temp-apps-crtl').unbind().click(function(e) {
            if (apps_list_mirror.length > 0) {
                $("#temp-apps-crtl span").text('Disable temporarily');
                $("#temp-apps-crtl > i").removeClass('fa-plus-circle');
                $("#temp-apps-crtl > i").addClass('fa-minus-circle');
                for (var i in apps_list_mirror) {
                    apps_list[i].enabled = apps_list_mirror[i];
                }

                apps_list_mirror = [];
                refreshAppsControl();
                port.postMessage({ message: 'toggle-temp-extensions', type: 'app', state: 'restore' });
            } else {
                for (var j in apps_list) {
                    apps_list_mirror.push(apps_list[j].enabled);
                }
                $("#temp-apps-crtl span").text('Restore');
                $("#temp-apps-crtl > i").removeClass('fa-minus-circle');
                $("#temp-apps-crtl > i").addClass('fa-plus-circle');
                port.postMessage({ message: 'toggle-temp-extensions', type: 'app', state: 'disable' });
            }
        });

        $('#searchApp').on('input', function(e) {
            filter = e.target.value;
            port.postMessage({ message: "get-apps", filter: filter });
        });

        $('#clearHistory').unbind().click(function(e) {
            var confirmHistory = confirm("This will clear all your browser history, are you sure?");
            if (confirmHistory) {
                port.postMessage({ message: 'clear-history' });
            }
        });

        $('#clearData').unbind().click(function(e) {
            chrome.tabs.create({ url: "chrome://settings/clearBrowserData", active: true });
        });

        $('#cleaner-select-all').unbind().click(function(e) {
            elms = $("input[name^=cleaner]").prop('checked', 'checked');
            $(this).addClass("disabled-control");
        });

        $("input[name^=cleaner]").on("change", function(e) {
            elms = $('input[name^=cleaner]');

            var checked = 0;
            for (var i = 0; i < elms.length; i++) {
                var elm = $(elms[i]);
                if (elm.prop("checked"))
                    checked++;
            }
            if (checked === 0) {
                $(".clean-button").addClass("disabled-control");
            } else {
                $(".clean-button").removeClass("disabled-control");
            }

            if (checked == elms.length) {
                $("#cleaner-select-all").addClass("disabled-control");
            } else {
                $("#cleaner-select-all").removeClass("disabled-control");
            }
        });

        $(".clean-button").unbind().click(function(e) {
            $('.pendingWindow').show();
            var confirmClean = confirm("This will clear all your browser data are you sure?");
            if (confirmClean) {
                period = $("#period").val();
                var elms = $('input[name^=cleaner]');
                arr = [];
                for (var i = 0; i < elms.length; i++) {
                    var elm = $(elms[i]);
                    if (elm.prop("checked"))
                        arr[i] = 1;
                    else
                        arr[i] = 0;
                }
                port.postMessage({ message: 'clean', values: arr, period: period });
            } else {
                $('.pendingWindow').hide();
            }
        });

        $("#autoclean-history, #autoclean-downloads, #autoclean-cache, #autoclean-cookies").unbind().click(function(e) {
            $(this).data('state', $(this).data('state') === 'enabled' ? 'disabled' : 'enabled');
            port.postMessage({ message: 'set-autoclean', data: { 
                history: {
                    state : $("#autoclean-history").data('state')
                },
                downloads: {
                    state : $("#autoclean-downloads").data('state')
                },
                cache: {
                    state : $("#autoclean-cache").data('state')
                },
                cookies: {
                    state : $("#autoclean-cookies").data('state')
                }
            } });
        });

        $("#cleaner-restore").unbind().click(function(e) {
            $("#cleaner-select-all").removeClass("disabled-control");
            var arr = [1, 1, 1, 1, 0, 0, 0];
            var elms = $('input[name^=cleaner]');
            for (var i = 0; i < elms.length; i++) {
                var elm = $(elms[i]);
                elm.prop("checked", arr[i] == 1);
            }
        });

        $('#searchHistory').on('input', function(e) {
            filter = e.target.value;
            port.postMessage({ message: "get-history", filter: filter });
        });

        $('#removeAllBookmarks').unbind().click(function(e) {
            var confirmBookmarks = confirm("This will clear all your bookmarks, are you sure?");
            if (confirmBookmarks) {
                port.postMessage({ message: 'remove-all-bookmarks' });
            }
        });

        $('#searchBookmarks').on('input', function(e) {
            filter = e.target.value;
            if (filter === "")
                port.postMessage({ message: "get-bookmarks" });
            else
                port.postMessage({ message: "search-bookmarks", filter: filter });
        });

        $(document).on("change", "#extensions input", function() {
            setExtensionState($(this).data('ext'), $(this).is(":checked") ? 'enabled' : 'disabled');
            filter = $('#searchExtension').value;
            port.postMessage({ message: 'enable-extension', extension: $(this).data('ext'), type: 'extension', enabled: $(this).is(":checked"), filter: filter });
        });

        $(document).on("click", "#extensions .delete-control", function() {
            setExtensionState($(this).data('ext'), 'deleted');
            filter = $('#searchExtension').value;
            port.postMessage({ message: 'uninstall-extension', extension: $(this).data('ext'), type: 'extension', filter: filter });
        });

        $(document).on("change", "#apps input", function() {
            setAppState($(this).data('ext'), $(this).is(":checked") ? 'enabled' : 'disabled');
            filter = $('#searchApp').value;
            port.postMessage({ message: 'enable-extension', extension: $(this).data('ext'), enabled: $(this).is(":checked"), type: 'app', filter: filter });
        });

        $(document).on("click", "#apps .delete-control", function() {
            setAppState($(this).data('ext'), 'deleted');
            filter = $('#searchApp').value;
            port.postMessage({ message: 'uninstall-extension', extension: $(this).data('ext'), type: 'app', filter: filter });
        });

        $(document).on("click", "#history .x-control", function() {
            filter = $('#searchHistory').value;
            var url = $(this).data("ext");
            port.postMessage({ message: 'delete-history', url: url, filter: filter });
        });

        $(document).on("click", "#bookmarks .x-control", function() {
            filter = $('#searchBookmarks').value;
            var id = $(this).attr("id");
            port.postMessage({ message: 'remove-bookmark', value: id, filter: filter });
        });

        $(document).on("click", "#bookmarks-results .x-control", function() {
            filter = $('#searchBookmarks').value;
            var id = $(this).attr("id");
            port.postMessage({ message: 'remove-bookmark', value: id, filter: filter });
        });

        $('.scroll-listing').scrollbar();
    });