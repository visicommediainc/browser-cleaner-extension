if (!browsercleaner) var browsercleaner = {};

browsercleaner.$main = {
    port: null,
    updateInterval: 24 * 60 * 60 * 1000, // 1 day
    beatInterval: 24 * 60 * 60 * 1000, // 1 day
    beatLastUpdate: 0,
    conflictsLastUpdate: 0,
    trackers: {},
    extFilter: "",
    appFilter: "",
    historyFilter: "",
    bookmarkFilter: "",
    onClicked: function(tab) {
        var url = chrome.extension.getURL('html/options.html');
        chrome.tabs.create({ url: url, active: true });
    },
    onMessage: function(request, sender, sendResponse) {
        switch (request.message) {}
    },
    returnedExtensions: function(port, type, filter) {
        if (!filter) {
            if (type == "extension")
                filter = browsercleaner.$main.extFilter;
            else
                filter = browsercleaner.$main.appFilter;
        }
        var filterExts = [];
        /*
        function warningFunc(permissionWarnings) {
            console.log(permissionWarnings);
        }
        */
        chrome.management.getAll(function(extensions) {
            for (var i = 0; i < extensions.length; i++) {
                var extension = extensions[i];
                if (extension.name === "Browser Cleaner")
                    continue;
                /*
                if (extension.name.indexOf("MyStart Shield") !== -1) {
                    console.log("extension Name:" + extension.name);
                    chrome.management.getPermissionWarningsById(extension.id, warningFunc);
                }

                if (extension.name.indexOf("New Tab") !== -1) {
                    console.log("extension Name:" + extension.name);
                    chrome.management.getPermissionWarningsById(extension.id, warningFunc);
                }
                */
                if (type === "app" && !extension.isApp)
                    continue;
                if (type === "extension" & extension.isApp)
                    continue;
                if (extension.name.toLowerCase().indexOf(filter) !== -1 || filter === "") {
                    filterExts.push(extension);
                }
            }
            port.postMessage({ message: type + "s-response", value: filterExts, start: type });
        });
    },
    onPopupMessage: function(port) {
        browsercleaner.$main.port = port;
        port.onMessage.addListener(function(request) {
            //console.log("message recieved:"+ JSON.stringify(request));
            switch (request.message) {
                case 'get-extensions':
                    {
                        filter = request.filter;
                        browsercleaner.$main.extFilter = filter;
                        browsercleaner.$main.returnedExtensions(port, "extension", filter);
                    }
                    break;
                case 'get-apps':
                    {
                        filter = request.filter;
                        browsercleaner.$main.appFilter = filter;
                        browsercleaner.$main.returnedExtensions(port, "app", filter);
                    }
                    break;
                case 'get-history':
                    {
                        filter = request.filter;
                        browsercleaner.$main.historyFilter = filter;
                        chrome.history.search({ text: filter, maxResults: 200 }, function(history) {
                            port.postMessage({ message: "history-response", value: history, start: 'history' });
                        });
                    }
                    break;
                case 'get-bookmarks':
                    {
                        chrome.bookmarks.getTree(function(bookmark) {
                            port.postMessage({ message: "bookmark-response", value: bookmark, start: 'bookmark' });
                        });
                    }
                    break;
                case 'get-autoclean':
                    {
                        chrome.storage.local.get("autoclean", function(result) {
                            port.postMessage({ message: "autoclean-response", data: (!result || !result.autoclean) ? null : result.autoclean});
                        });
                    }
                    break;
                case 'set-autoclean':
                    {
                        chrome.storage.local.set({"autoclean": request.data}, function() {});
                    }
                    break;
                case 'search-bookmarks':
                    {
                        filter = request.filter;
                        browsercleaner.$main.bookmarkFilter = filter;
                        chrome.bookmarks.search(filter, function(bookmark) {
                            port.postMessage({ message: "bookmark-response", value: bookmark, start: 'bookmark' });
                        });
                    }
                    break;
                case 'enable-extension':
                    {
                        extId = request.extension;
                        enabled = request.enabled;
                        type = request.type;
                        chrome.management.setEnabled(extId, enabled, function() {});
                    }
                    break;
                case 'uninstall-extension':
                    {
                        extId = request.extension;
                        type = request.type;
                        chrome.management.uninstall(extId, { showConfirmDialog: false }, function() {
                            browsercleaner.$main.returnedExtensions(port, type);
                        });
                    }
                    break;
                case 'remove-all-extensions':
                    {
                        chrome.management.getAll(function(extensions) {
                            for (var i = 0; i < extensions.length; i++) {
                                var extension = extensions[i];
                                if (extension.name === "Browser Cleaner")
                                    continue;
                                if (extension.isApp)
                                    continue;
                                extId = extension.id;
                                chrome.management.uninstall(extId, { showConfirmDialog: false }, function() {});
                            }
                            port.postMessage({ message: "extensions-response", value: [], start: 'extension' });
                        });
                    }
                    break;
                case 'remove-all-apps':
                    {
                        chrome.management.getAll(function(extensions) {
                            for (var i = 0; i < extensions.length; i++) {
                                var extension = extensions[i];
                                if (!extension.isApp)
                                    continue;
                                extId = extension.id;
                                chrome.management.uninstall(extId, { showConfirmDialog: false }, function() {});
                                port.postMessage({
                                    message: "extensions-response",
                                    value: [],
                                    start: 'app'
                                });
                            }
                        });
                    }
                    break;
                case 'disable-all-extensions':
                    {
                        type = request.type;
                        chrome.management.getAll(function(extensions) {
                            for (var i = 0; i < extensions.length; i++) {
                                var extension = extensions[i];
                                if (type == "extension" && extension.name === "Browser Cleaner")
                                    continue;
                                if (type == "extension" && extension.isApp)
                                    continue;
                                if (type == "app" && !extension.isApp)
                                    continue;
                                extId = extension.id;
                                chrome.management.setEnabled(extId, false, function() {});
                            }
                            port.postMessage({ message: "extensions-response", value: extensions, start: type });
                        });
                    }
                    break;
                case 'toggle-temp-extensions':
                    {
                        state = request.state;
                        type = request.type;
                        var exts = [];
                        if (state == "restore") {
                            chrome.storage.local.get("extensions", function(result) {
                                if (result.extensions) {
                                    extensions = result.extensions;
                                    for (var i = 0; i < extensions.length; i++) {
                                        extension = extensions[i];
                                        extId = extension.id;
                                        enabled = extension.enabled;
                                        chrome.management.setEnabled(extId, enabled, function() {});
                                    }
                                    chrome.storage.local.remove("extensions");
                                    port.postMessage({ message: "extensions-response", value: extension, start: 'extension' });
                                }
                            });
                        } else {
                            chrome.management.getAll(function(extensions) {
                                chrome.management.getAll(function(extensions) {
                                    for (var i = 0; i < extensions.length; i++) {
                                        var extension = extensions[i];
                                        if (type == "extension" && extension.name === "Browser Cleaner")
                                            continue;
                                        if (type == "extension" && extension.isApp)
                                            continue;
                                        if (type == "app" && !extension.isApp)
                                            continue;
                                        extId = extension.id;
                                        exts.push(extension);
                                        chrome.management.setEnabled(extId, false, function() {});
                                    }
                                    chrome.storage.local.set({
                                        extensions: exts
                                    }, function() {});
                                    browsercleaner.$main.port.postMessage({ message: "extensions-response", value: exts, start: 'extension' });
                                });
                            });
                        }
                    }
                    break;
                case 'get-conflicts':
                    {
                        conflicts = [];
                        chrome.management.getAll(function(extensions) {
                            for (i = 0; i < extensions.length; i++) {
                                for (j = 0; j < browsercleaner.$main.tracker.list.length; j++) {
                                    if (extensions[i].id === browsercleaner.$main.tracker.list[j]) {
                                        conflicts.push(extensions[i]);
                                    }
                                }
                            }
                            port.postMessage({ message: "conflicts-response", value: conflicts, start: 'conflict' });
                        });
                    }
                    break;
                case 'clear-history':
                    {
                        chrome.history.deleteAll(function() {
                            alert("History has been delete!");
                            port.postMessage({ message: "history-response", value: [], start: 'history' });
                        });
                    }
                    break;
                case 'delete-history':
                    {
                        chrome.history.deleteUrl({ url: request.url }, function() {
                            chrome.history.search({ text: '' }, function(history) {
                                port.postMessage({ message: "history-response", value: history, start: 'history' });
                            });
                        });
                    }
                    break;
                case 'remove-bookmark':
                    {
                        chrome.bookmarks.remove(request.value, function() {
                            chrome.bookmarks.getTree(function(bookmark) {
                                port.postMessage({ message: "bookmark-response", value: bookmark, start: 'bookmark' });
                            });
                        });
                    }
                    break;
                case 'remove-all-bookmarks':
                    {
                        chrome.bookmarks.getTree(function(bookmarks) {
                            bookmarks.forEach(function(element) {
                                element.children.forEach(function(bookmark) {
                                    bookmark.children.forEach(function(entry) {
                                        chrome.bookmarks.remove(entry.id, function() {
                                            port.postMessage({ message: "bookmark-response", value: [], start: 'bookmark' });
                                        });
                                    });
                                });
                            });
                        });
                    }
                    break;
                case 'clean':
                    {
                        list = request.values;
                        period = request.period;
                        switch (period) {
                            case 'hour':
                                millisecondsPerTime = 1000 * 60 * 60;
                                break;
                            case 'day':
                                millisecondsPerTime = 1000 * 60 * 60 * 24 * 1;
                                break;
                            case 'week':
                                millisecondsPerTime = 1000 * 60 * 60 * 24 * 7;
                                break;
                            case '4weeks':
                                millisecondsPerTime = 1000 * 60 * 60 * 24 * 7 * 4;
                                break;
                            case 'beginning':
                                millisecondsPerTime = 1000 * 60 * 60 * 24 * 7 * 1000;
                                break;
                        }
                        var since = (new Date()).getTime() - millisecondsPerTime;
                        chrome.browsingData.remove({
                            "since": since
                        }, {
                            "appcache": true,
                            "cache": list[2] == 1,
                            "cookies": list[3] == 1,
                            "downloads": list[1] == 1,
                            "fileSystems": true,
                            "formData": list[5] == 1,
                            "history": list[0] == 1,
                            "indexedDB": true,
                            "localStorage": list[4] == 1,
                            "pluginData": true,
                            "passwords": list[6] == 1,
                            "webSQL": true
                        }, function() {
                            port.postMessage({ message: "clean-response", start: 'clean' });
                        });
                    }
                    break;
            }
        });
    },
    onEnabledDisabled: function(ExtensionInfo) {
        if (ExtensionInfo.type === 'extension') {
            filter = browsercleaner.$main.extFilter;
            type = "extension";
        } else {
            filter = browsercleaner.$main.appFilter;
            type = "app";
        }

        browsercleaner.$main.returnedExtensions(browsercleaner.$main.port, type, filter);
    },
    getRemoteInfo: function(url, callback, type) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if (type === "arraybuffer") {
                var arrayBuffer = xhr.response;
                if (arrayBuffer) {
                    var byteArray = new Uint8Array(arrayBuffer);
                    callback(browsercleaner.$main.bin2String(byteArray));
                    arrayBuffer = null;
                    byteArray = null;
                    xhr = null;
                }
            } else {
                callback(xhr.responseText);
                xhr = null;
            }
        };
        if (type === "arraybuffer")
            xhr.responseType = "arraybuffer";

        xhr.open("GET", url, true);
        xhr.send();
    },
    checkAutoClean: function() {
        chrome.storage.local.get("autoclean", function(result) {
            if(!result || !result.autoclean) {
                return;
            }

            //TC-124 Add Auto Clean at startup. Time range use "the past day".
            chrome.browsingData.remove({
                "since": 1000 * 60 * 60 * 24 * 1
            }, {
                "history": (result.autoclean.history.state === 'enabled' ? true : false),
                "downloads": (result.autoclean.downloads.state === 'enabled' ? true : false),
                "cache": (result.autoclean.cache.state === 'enabled' ? true : false),
                "cookies": (result.autoclean.cookies.state === 'enabled' ? true : false),
            }, function() { });
        });
    },
    $init: function() {
        console.log("init");
        var now = parseInt(new Date().getTime() / 1000);
        chrome.storage.local.get("beatLastUpdate", function(result) {
            browsercleaner.$main.beatLastUpdate = (typeof result.beatLastUpdate !== "undefined") ? result.beatLastUpdate : 0;
            beatUpdateDue = parseInt(browsercleaner.$main.beatLastUpdate) + parseInt(browsercleaner.$main.beatInterval / 1000);
            if (beatUpdateDue > now) {
                console.log("Too soon to send heart beat!");
                return;
            }
            setTimeout(function() {
                browsercleaner.$analytics.trackEvent("Heartbeat_DailyUser", browsercleaner.$broker.campaign_id, " ", " ");
            }, 1000);
            chrome.storage.local.set({ "beatLastUpdate": now }, function() {
                browsercleaner.$main.beatLastUpdate = now;
                console.log("beatLastUpdate sent at " + now);
            });
        });

        chrome.storage.local.get("conflictsLastUpdate", function(result) {
            browsercleaner.$main.conflictsLastUpdate = (typeof result.conflictsLastUpdate !== "undefined") ? result.conflictsLastUpdate : 0;
            conflictsUpdateDue = parseInt(browsercleaner.$main.conflictsLastUpdate) + parseInt(browsercleaner.$main.updateInterval / 1000);
            chrome.storage.local.get("tracker", function(result1) {
                try {
                    browsercleaner.$main.tracker = JSON.parse(result1.tracker) || {};
                } catch (ex) {
                    //console.log("settings.error:" + ex.message);
                    browsercleaner.$main.tracker = {};
                }
            });

            if (conflictsUpdateDue > now) {
                console.log("Too soon to update tracking list. Keeping old list");
                return;
            }
            browsercleaner.$main.getRemoteInfo(browsercleaner.$settings.CONFLICTS, function(tracker_str) {
                try {
                    chrome.storage.local.set({ "tracker": tracker_str }, function() {
                        var tacker_json = JSON.parse(tracker_str);
                        browsercleaner.$main.tracker = tacker_json;
                        console.log("tracker saved at " + new Date());
                    });

                    chrome.storage.local.set({ "conflictsLastUpdate": now }, function() {
                        browsercleaner.$main.conflictsLastUpdate = now;
                        console.log("conflictsLastUpdate saved at " + now);
                    });
                } catch (e) {
                    console.log("something went wrong here...not updating trackers.error:" + e.message);
                }
            });
        });

    }
};


document.addEventListener("DOMContentLoaded", function() {
    browsercleaner.$main.$init();
    browsercleaner.$main.checkAutoClean();
    setInterval(function() { browsercleaner.$main.$init(); }, browsercleaner.$main.updateInterval);
    chrome.extension.onMessage.addListener(browsercleaner.$main.onMessage);
    chrome.runtime.onConnect.addListener(browsercleaner.$main.onPopupMessage);
    chrome.browserAction.onClicked.addListener(browsercleaner.$main.onClicked);
    chrome.management.onEnabled.addListener(browsercleaner.$main.onEnabledDisabled);
    chrome.management.onDisabled.addListener(browsercleaner.$main.onEnabledDisabled);
});