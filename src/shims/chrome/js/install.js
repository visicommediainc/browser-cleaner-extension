if (chrome.extension)
{
    var port = chrome.extension.connect({name: "pornblocker"});
    port.onMessage.addListener(function(request) {
        if (!request.message ) return;
        switch(request.message)
        {
            case 'conflicts-response':
            {
                if (!request.value)
                    return;
                 var conflicts = request.value;
                $('#conflicts').empty();
                expandConflicts(conflicts);               
            }
            break;
        }
    });
}
    
function expandConflicts(conflicts)
{
    if (conflicts.length > 0)
    {
         $("#tabs-conflicts").show();
    }
    for (i = 0; i < conflicts.length; i++)
    {
        var extType     = conflicts[i].type;
        var extId       = conflicts[i].id;
        var extEnabled  = conflicts[i].enabled;
        var extIco      = "<i class='glyphicon glyphicon-file'></i>";
        if(typeof  conflicts[i].icons !== "undefined")
        {
            extIco ='<img src="'+conflicts[i].icons[0].url+'" border=0 width="16">';	
        }
        var act="";
        var cl_name = "my_disabled";
        if(extEnabled)
        {
            act="<i class='glyphicon glyphicon-ban-circle white' data-toggle='tooltip' title='Disable'></i>";
            cl_name = "my_active";
        }
        $("#conflicts").append("<li id='"+extId+"' class='list-group-item "+cl_name+"'>"+extIco+"<span class='name'> "+conflicts[i].name+" </span><span class='badge'><a href='#' class='disable'>"+act+"</a> <a href='#' class='delete' data-toggle='tooltip' title='Delete'><i class='glyphicon glyphicon-trash white'></i></a></span> </li>");
    }
}

function onLoad(){
    if (port)
    {
        port.postMessage({message: "get-conflicts"});
    }
    $(document).on("click",".disable",function(e) {
        e.preventDefault();
        var id=$(this).closest('li').attr("id");
        var i=$(this).find("i");
        var enabled = false;
        port.postMessage({message:'enable-extension', extension: id, enabled: enabled});
        port.postMessage({message: "get-conflicts"});
    });

    $(document).on("click", ".delete" , function(e) {
        e.preventDefault();
        var id=$(this).closest('li').attr("id");
        port.postMessage({message:'uninstall-extension', extension: id});
    });

}

document.addEventListener("DOMContentLoaded", onLoad, false);
