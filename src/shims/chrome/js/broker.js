if (!browsercleaner)
    var browsercleaner = {};
browsercleaner.$broker = {
    hostName: 'com.mystart.one.newtab.' + chrome.runtime.id,
    campaign_id: "",
    user_id: "",
    getCampaignId: function(callback) {
        chrome.runtime.sendNativeMessage(browsercleaner.$broker.hostName, {
            endpoint: 'get-campaign-id'
        }, function(response) {
            if (chrome.runtime.lastError) {
                response = {
                    error: 'BROKER_NOT_FOUND',
                    campaign_id: null
                };
            }

            callback(response);
        });
    },
    getUserId: function(callback) {
        chrome.runtime.sendNativeMessage(browsercleaner.$broker.hostName, {
            endpoint: 'get-user-id'
        }, function(response) {
            if (chrome.runtime.lastError) {
                response = {
                    error: 'BROKER_NOT_FOUND',
                    user_id: null
                };
            }

            callback(response);
        });
    },
    guid: function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    },
    $init: function() {
        console.log("broker init");
        browsercleaner.$broker.user_id = browsercleaner.$broker.guid();
        browsercleaner.$broker.campaign_id = "123";
        chrome.storage.local.get("campaign_id", function(result) {
            if (result.campaign_id)
                browsercleaner.$broker.campaign_id = result.campaign_id;
        });
        chrome.storage.local.get("user_id", function(result) {
            if (result.user_id)
                browsercleaner.$broker.user_id = result.user_id;
        });

        browsercleaner.$broker.getCampaignId(function(data) {
            if (!data.error && data.campaign_id) {
                browsercleaner.$broker.campaign_id = data.campaign_id;
                chrome.storage.local.set({ "campaign_id": browsercleaner.$broker.campaign_id });
                chrome.runtime.setUninstallURL(browsercleaner.$settings.UNINSTALL_URL + browsercleaner.$broker.campaign_id, function() {});
            }
        });

        browsercleaner.$broker.getUserId(function(data) {
            if (!data.error && data.user_id) {
                browsercleaner.$broker.user_id = data.user_id;
                chrome.storage.local.set({ "user_id": browsercleaner.$broker.user_id });
            }
        });
        chrome.runtime.onInstalled.addListener(function(details) {
            if (details.reason == "install") {
                setTimeout(function() {
                    browsercleaner.$analytics.trackEvent("Runtime_Install", browsercleaner.$broker.campaign_id, " ", " ");
                    chrome.tabs.create({
                        url: browsercleaner.$settings.INSTALLED_URL
                    });

                }, 1000);
                chrome.runtime.setUninstallURL(browsercleaner.$settings.UNINSTALL_URL + browsercleaner.$broker.campaign_id, function() {});
            }
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    browsercleaner.$broker.$init();
});