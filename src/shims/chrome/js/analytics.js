if (!browsercleaner)
    var browsercleaner = {};

browsercleaner.$analytics = {
    $ga: {
        _AnalyticsCode: "UA-96927833-2",
        init: function() {},
        trackPage: function(page, customUrl) {
            var request = new XMLHttpRequest();
            var message =
                "v=1&tid=" + browsercleaner.$analytics.$ga._AnalyticsCode + "&cid=" + browsercleaner.$broker.user_id + "&aip=1" +
                "&cn=" + browsercleaner.$events.campaign_id + "&ds=add-on&t=pageview&dp=" + page + "&dr=" + customUrl;

            console.log("trackPage.message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        },
        trackEvent: function(category, action, label, value) {
            var request = new XMLHttpRequest();
            var message = "tid=" + browsercleaner.$analytics.$ga._AnalyticsCode + "&cid=" + browsercleaner.$broker.user_id;
            message += "&cn=" + browsercleaner.$broker.campaign_id + "&t=event&ec=" + category + "&v=1";

            if (action && action !== '')
                message += "&ea=" + action;

            if (label && label !== '')
                message += "&el=" + label;

            console.log("ga.trackEvent.message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        }
    },
    $piwik: {
        url: "https://analytics.vmn.net/",
        cid: 15,
        pwTracker: {},
        getPwTracker: function(baseUrl, siteId) {
            var u = baseUrl.replace(/\/$/, "");
            var tracker = function() {
                (window._paq = window._paq || []).push(Array.prototype.slice.call(arguments));
            };
            tracker = Piwik.getTracker(u + '/piwik.php', siteId);
            return tracker;
        },
        init: function() {
            browsercleaner.$analytics.$piwik.pwTracker = browsercleaner.$analytics.$piwik.getPwTracker(browsercleaner.$analytics.$piwik.url, browsercleaner.$analytics.$piwik.cid);
        },
        trackPage: function(page, customUrl) {
            var url = 'https://browsercleaner.com/?pk_campaign=' + browsercleaner.$broker.campaign_id;
            browsercleaner.$analytics.$piwik.pwTracker.setUserId(browsercleaner.$broker.user_id);
            browsercleaner.$analytics.$piwik.pwTracker.setCustomUrl(url);
            browsercleaner.$analytics.$piwik.pwTracker.setDocumentTitle(page); // replace wlcome with the page name
            browsercleaner.$analytics.$piwik.pwTracker.trackPageView(); // replace welcome with page name
        },
        trackEvent: function(category, action, label, value) {
            var url = 'https://browsercleaner.com/?pk_campaign=' + browsercleaner.$broker.campaign_id;
            console.log("piwik.trackEvent.url=" + url);
            browsercleaner.$analytics.$piwik.pwTracker.setUserId(browsercleaner.$broker.user_id);
            browsercleaner.$analytics.$piwik.pwTracker.setCustomUrl(url);
            browsercleaner.$analytics.$piwik.pwTracker.trackEvent(category, action, label, value);
        }
    },
    trackPage: function(page, customUrl) {
        browsercleaner.$analytics.$ga.trackPage(page, customUrl);
        browsercleaner.$analytics.$piwik.trackPage(page, customUrl);
    },
    trackEvent: function(category, action, label, value) {
        browsercleaner.$analytics.$ga.trackEvent(category, action, label, value);
        browsercleaner.$analytics.$piwik.trackEvent(category, action, label, value);
        console.log("trackEvent:" + category + "|" + action + "|" + label + "|" + value);
    }
};

document.addEventListener("DOMContentLoaded", function() {
    browsercleaner.$analytics.$ga.init();
    browsercleaner.$analytics.$piwik.init();
});